The WSDS 2018 poster file is WSDS_2018_Doc.Rmd. To view the published version, go to https://lindseydietz.shinyapps.io/WSDS_2018_LDietz/ 

Questions? Feel free to email me at diet0146@umn.edu.